import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class ChineseRemainderTheorem {
    public static BigInteger[] exgcd(BigInteger a, BigInteger b){ // Extended Euclidean Algorithm using BigIntegers
        if (b.equals(BigInteger.valueOf(0)))
            return new BigInteger[] {a, BigInteger.valueOf(1), BigInteger.valueOf(0)};
        BigInteger[]c = exgcd(b, a.mod(b));
        return new BigInteger[] {c[0], c[2], c[1].subtract(a.divide(b).multiply(c[2]))};
    }
    public static BigInteger chineseReminder(List<BigInteger> num, BigInteger M, List<BigInteger> mods){
        BigInteger ans = BigInteger.valueOf(0); // Evaluating ans = num[0] * M0 * Inverse(M0, Mod = m0)  +  num[1] * M1 * Inverse(M1, Mod = m1) + ....  to solve the congruent equations
        for(int i = 0; i < mods.size(); i++){
            BigInteger mi = M.divide(mods.get(i)); // calculating Mi
            BigInteger inv  = (exgcd(mi , mods.get(i))[1].add(mods.get(i))).mod(mods.get(i));  ///Inverse(Mi, Mod = mi)
            ans = ans.add(num.get(i).multiply(mi.multiply(inv))); // adding to the ans;
        }
        return ans.mod(M); // Mod to get the representation in ZM
    }
    public static BigInteger[] solve(List<BigInteger> mods, BigInteger a, BigInteger b){
        BigInteger m = BigInteger.valueOf(1);
        for (BigInteger md : mods) // Multiply all mods together
            m = m.multiply(md);
        List<BigInteger> A = new ArrayList<>(); // Representation of A in n-Tuple form
        List<BigInteger> B = new ArrayList<>();// Representation of B in n-Tuple form
        for(BigInteger md : mods){
            A.add(a.mod(md));
            B.add(b.mod(md));
        }
        List<BigInteger> sum = new ArrayList<>(); // Representation of Sum  in n-Tuple form
        List<BigInteger> multiply =  new ArrayList<>();// Representation of Multiplication  in n-Tuple form
        for(int i = 0; i < A.size(); i++){
            sum.add((A.get(i).add(B.get(i))).mod(mods.get(i))); // adding component wise of the two numbers
            multiply.add((A.get(i).multiply(B.get(i))).mod(mods.get(i))); // multiplying component wise of two numbers
        }
        return new BigInteger[]{chineseReminder(sum, m, mods), chineseReminder(multiply, m, mods)}; // Solving congruent equations
    }
}
