public class FastExponentiation {
    public static long naive1(long a, long b, long m){
        long c = 1; // result
        for (long i = 0; i < b; i++){
            c *= a; // multiply a  b amount of times, Overflow happens here LONG_MAX
        }
        c %= m;  // taking mod operation
        return  c;
    }
    public static long naive2(long a, long b, long m){
        long c = 1; //  result
        for (long i = 0; i < b; i++){
            c = (c*a)%m; // multiply a  b amount of times, Overflow happens here; if m^2 > LONG_MAX and c*a might exceed LONG_MAX
        }
        return  c;
    }
    public static long iterative(long a, long b, long m){
        long c = 1;//result
        while (b > 0){
            if(b%2 == 1){// if bit is one in the binary representation of b, Multiply the result
                c  = (c*a)%m;
            }
            a = (a*a)%m; // get a^(2^i) by squaring a^(2^(i-1));
            b = b /2; // next bit
        }
        return c;
    }
    public static long recursive(long a, long b, long m){
        if (b == 0)//base case
            return 1;//a^0 = 1
        if (b%2 == 1)//if odd
            return (a * recursive(a,b-1, m)) % m; // take one a out and then the power is even, calculate the result
        long c = recursive(a, b/2, m); // C: is a ^ (b/2)
        return (c*c)%m; // calculate a^b by squaring a^(b/2)
    }
}
