public class ExtendedEuclidean {
    // Format s*a + t*b = gcd
    public static long[] gcd(long a, long b){
        if (b == 0)
            return new long[] {a, 1, 0}; // base case s = 1, t = 0
        long[]c = gcd(b, a%b); // Answer of Next call
        // Recurrence equation: s* = t , t* = s - (a/b)*t
        return new long[] {c[0], c[2], c[1]-(a/b)*c[2]}; // Answer to prev call
    }
}
