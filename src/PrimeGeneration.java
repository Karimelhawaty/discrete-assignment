import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PrimeGeneration {
    public static List<Integer> sieve(int n){
        boolean[] isPrime = new boolean[n+5];
        Arrays.fill(isPrime, true); // Assume every number is a prime number
        for (int i = 2, d = 1;(long) i * i <= n; i+=d, d = 2){ // Iterate until the sqrt of n, Since for every  composite number there must bea  divisor less than sqrt(num)
            if (isPrime[i]){ // If the number is prime
                for (int j = i*i; j <= n; j+=i){ // Iterate through the multiples of the current prime
                    isPrime[j] = false; // All multiples of the prime are composite so they aren't prime
                }
            }
        }
        List<Integer> list = new ArrayList<>(); // answer List
        for(int i = 2; i <= n; i++){
            if(isPrime[i]) { // Add it to the list if it is prime
                list.add(i);
            }
        }
        return list;
    }
}
