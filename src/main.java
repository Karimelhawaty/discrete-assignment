import java.math.BigInteger;
import java.sql.Time;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class main {
    public static BigInteger getRand(){
        BigInteger maxLimit = new BigInteger("500000000000000000000");
        BigInteger minLimit = new BigInteger("0");
        BigInteger bigInteger = maxLimit.subtract(minLimit);
        Random randNum = new Random();
        int len = maxLimit.bitLength();
        BigInteger res = new BigInteger(len, randNum);
        if (res.compareTo(minLimit) < 0)
            res = res.add(minLimit);
        if (res.compareTo(bigInteger) >= 0)
            res = res.mod(bigInteger).add(minLimit);
        return res;
    }
    public static void main(String[] args){
        // 101110 = 46 -> 6 bits; mod = 1e9+7
        Random rand = new Random();
        List<BigInteger> list = Arrays.asList(new BigInteger[]{
                BigInteger.valueOf(FastExponentiation.iterative(2, 35, Long.MAX_VALUE)-1),
                BigInteger.valueOf(FastExponentiation.iterative(2, 34, Long.MAX_VALUE)-1),
                BigInteger.valueOf(FastExponentiation.iterative(2, 33, Long.MAX_VALUE)-1),
                BigInteger.valueOf(FastExponentiation.iterative(2, 31, Long.MAX_VALUE)-1),
                BigInteger.valueOf(FastExponentiation.iterative(2, 29, Long.MAX_VALUE)-1),
                BigInteger.valueOf(FastExponentiation.iterative(2, 23, Long.MAX_VALUE)-1)

         });
        for(int i = 0; i < 10; i++){
            BigInteger A = getRand();
            BigInteger B = getRand();
            long start = System.nanoTime();
            BigInteger[] ans = ChineseRemainderTheorem.solve(list, A, B);
            long end = System.nanoTime();
            System.out.println("A = " + A + ", B = " + B);
            System.out.println("Using Chinese Remainder Theorem to calculate The Sum and Multiplication of two Numbers: ");
            System.out.println("Sum answer: " + ans[0]);
            System.out.println("Multiplication answer: " + ans[1]);
            System.out.println("Time taken in ns: " + (end-start));
            System.out.println("Time taken in ms: " + TimeUnit.NANOSECONDS.toMillis(end-start));
            System.out.println("Using BigInteger class to calculate The Sum and Multiplication of two Numbers: ");
            start = System.nanoTime();
            BigInteger sum = A.add(B);
            BigInteger multiplication = A.multiply(B);
            end = System.nanoTime();
            System.out.println("Sum answer: " + sum);
            System.out.println("Multiplication answer: " + multiplication);
            System.out.println("Time taken in ns: " + (end-start));
            System.out.println("Time taken in ms: " + TimeUnit.NANOSECONDS.toMillis(end-start));
            System.out.println();

        }


    }
}
